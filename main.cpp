#include <iostream>

using namespace std;

void output_input_parameters(int argc, char** argv) {
    cout << "Number of input parameters = " << argc << endl;
    for (int i = 0; i < argc; i++) {
        cout << argv[i] << endl;
    }
}

int main(int argc, char** argv) {
    output_input_parameters(argc, argv);
    cout << "Hello, World!" << endl;
    return 0;
}

